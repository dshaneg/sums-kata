namespace Sums;

public static class SumCheckerYieldSum
{
    public static bool DoesSumExist(int sum, int[] nums)
    {
        foreach(var calculatedSum in FindSums(nums))
            if (calculatedSum == sum)
                return true;

        return false;
    }

    private static IEnumerable<int> FindSums(int[] nums)
    {
        for (int i = 0; i < nums.Length; i++)
            for (int j = i + 1; j < nums.Length; j++)
                yield return nums[i] + nums[j];
    }
}
