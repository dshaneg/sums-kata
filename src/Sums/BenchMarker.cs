using BenchmarkDotNet.Attributes;

namespace Sums;

[MemoryDiagnoser]
public class BenchMarker
{
    private readonly int _sum = -1;
    private readonly int[] _input = new int[] { 5, 2, 3, 200, 4, 15, 2323, 47, 11, 4843, 42, 1, 32767, -999, 0 };

    [Benchmark(Baseline = true)]
    public void RunSumChecker()
    {
        SumChecker.DoesSumExist(_sum, _input);
    }

    [Benchmark]
    public void RunSumCheckerPairClass()
    {
        SumCheckerPairClass.DoesSumExist(_sum, _input);
    }

    [Benchmark]
    public void RunSumCheckerPairStruct()
    {
        SumCheckerPairStruct.DoesSumExist(_sum, _input);
    }

    [Benchmark]
    public void RunSumCheckerYieldSum()
    {
        SumCheckerYieldSum.DoesSumExist(_sum, _input);
    }
}
