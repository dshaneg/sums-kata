namespace Sums;

public static class SumCheckerPairClass
{
    public static bool DoesSumExist(int sum, int[] nums)
    {
        foreach(var pair in FindPairs(nums))
            if (pair.Sum == sum)
                return true;

        return false;
    }

    private static IEnumerable<Pair> FindPairs(int[] nums)
    {
        for (int i = 0; i < nums.Length; i++)
            for (int j = i + 1; j < nums.Length; j++)
                yield return new Pair(nums[i], nums[j]);
    }

    private class Pair
    {
        public int Augend;
        public int Addend;
        public int Sum;

        public Pair(int augend, int addend)
        {
            Augend = augend;
            Addend = addend;
            Sum = augend + addend;
        }
    }
}
