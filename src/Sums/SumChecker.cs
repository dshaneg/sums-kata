namespace Sums;

public static class SumChecker
{
    public static bool DoesSumExist(int sum, int[] nums)
    {
        for (int i = 0; i < nums.Length; i++)
            for (int j = i + 1; j < nums.Length; j++)
                if (nums[i] + nums[j] == sum)
                    return true;


        return false;
    }
}
