# Sums

This kata is based an interview exercise where the applicant has to "find the problem" with a particular implementation of the following requirements.
This solution is an attempt to build the exercise using TDD.

## Requirements

Write a function `DoesSumExist` that accepts `int sum` and `int[] nums`, and returns `boolean`. The function should return true if any two *distinct* elements of the unsorted array `nums` add up to `sum`.

For example, given

* sum = 20
* nums = [5, 2, 3, 200, 4, 15]

the result would be `true` because 5 + 15 = 20.

However, given

* sum = 20
* [1, 10]

the result should be `false` because we shouldn't add 10 to itself for the sum check.
