using Sums;

using Xunit;

namespace UnitTests;

public class DoesSumExistTests
{
    [Theory]
    [InlineData(0, new int[] { 0, 0 })] // first test
    [InlineData(1, new int[] { 0, 1 })] // second test
    [InlineData(2, new int[] { 0, 1, 1 })] // sixth test
    [InlineData(2, new int[] { 1, 0, 1 })] // seventh test
    [InlineData(20, new int[] { 5, 2, 3, 200, 4, 15 })] // acceptance test
    [InlineData(0, new int[] { 1, -1 })] // post-tdd. of course negative numbers work...
    public void ShouldReturnTrue(int sum, int[] nums)
    {
        bool exists = SumChecker.DoesSumExist(sum, nums);

        Assert.True(exists);
    }

    [Theory]
    [InlineData(0, new int[] { })] // third test
    [InlineData(0, new int[] { 0 })] // fourth test
    [InlineData(1, new int[] { 0, 0 })] // fifth test
    [InlineData(20, new int[] { 1, 10 })] // eighth test - acceptance test
    [InlineData(20, new int[] { 1, 10, 0 })] // ninth test
    [InlineData(0, new int[] { int.MaxValue, int.MaxValue })] // post-tdd, doesn't overflow, but can't be checked with an int
    public void ShouldReturnFalse(int sum, int[] nums)
    {
        bool exists = SumChecker.DoesSumExist(sum, nums);

        Assert.False(exists);
    }
}
