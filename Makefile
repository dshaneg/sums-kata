#help:	@ List available tasks on this project
help:
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(MAKEFILE_LIST) | tr -d '#' | awk 'BEGIN {FS = ":.*?@ "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#test:	@ Run unit tests
test :
	dotnet test tests/UnitTests --nologo --no-restore

#run:	@ Execute the program
run:
	dotnet run --project src/Sums/

bench:
	dotnet run --project src/Sums/ --configuration Release
